# Ludumdare 42
#### Open Window Game Jam

## Resources
* **Google Classroom Code:** hmht9te
* [**Realtime Board:**](https://realtimeboard.com/app/board/o9J_kzNugyY=/)
* [**LD Jam Page**](https://ldjam.com/events/ludum-dare/42/$103041)
* [**Discord**](https://discord.gg/vcHsw5T) - Team chat

## Contribution
* Create a personal branch. 
* Pull necessary content from other branches whilst still in development.
* Once feature is complete, merge to Dev/Art branch for testing.
* Once benchmark has been reached, merge to main branch.

**Main branch should always be stable**