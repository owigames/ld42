﻿using UnityEngine;

public enum CrateType
{
    Light,
    Heavy
}

public class CrateSpawner : MonoBehaviour
{
    [SerializeField] private GameObject lightCratePrefab;
    [SerializeField] private GameObject heavyCratePrefab;
    [SerializeField] private Transform crateSpawnPoint;

    public void SpawnCrate(CrateType crateType)
    {
        GameObject crate;

        switch (crateType)
        {
            case CrateType.Light:
                crate = GameObject.Instantiate(lightCratePrefab, crateSpawnPoint.position, Quaternion.identity);
                break;
            case CrateType.Heavy:
                crate = GameObject.Instantiate(heavyCratePrefab, crateSpawnPoint.position, Quaternion.identity);
                break;
            default:
                break;
        }
    }
}
