﻿using Rewired;
using UnityEngine;

public class CrateGrabber : MonoBehaviour
{
    public LayerMask pickUpLayer;

    public Transform pickUpLocation;

    public PlayerSelection _PlayerSelection;
    public int playerID;

    private Player _Input;
    private bool didPickUp;
    private GameObject pickUpTarget;
    private Collider pickUpCollider;
    private Collider intersectCollider;

    void Awake()
    {
        playerID = (_PlayerSelection.GetHashCode());
        _Input = ReInput.players.GetPlayer(playerID);
        pickUpCollider = pickUpLocation.GetComponent<Collider>();
    }

    private void Update()
    {
		InteractCtrl ("Interact");
    }

    void InteractCtrl(string button)
    {
        if (_Input.GetButtonDown(button))
        {
            //---YOU NEED TO DROP---
            if (didPickUp)
            {
                pickUpTarget.GetComponent<Rigidbody>().isKinematic = false;
                pickUpTarget.GetComponent<Collider>().enabled = true;
                pickUpLocation.transform.DetachChildren();
                didPickUp = false;
            }

            //---YOU NEED TO PICKUP---
            else if (!didPickUp)
            {
                Collider[] hitColliders = Physics.OverlapSphere(pickUpLocation.position, 1, pickUpLayer);

                if (hitColliders.Length > 0)
                {
                    if (hitColliders[0].GetComponent<Crate>().IsHeavy && !GameManager.Instance.PopeyeSpinach)
                        return;

                    pickUpTarget = hitColliders[0].gameObject;
                    pickUpTarget.transform.position = pickUpLocation.position;
                    pickUpTarget.GetComponent<Collider>().enabled = false;
                    pickUpTarget.GetComponent<Rigidbody>().isKinematic = true;
                    pickUpTarget.transform.SetParent(pickUpLocation.transform);

                    didPickUp = true;
                }
            }
        }
    }
}
