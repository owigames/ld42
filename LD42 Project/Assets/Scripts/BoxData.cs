﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;

[CreateAssetMenu (fileName = "BoxData", menuName = "Boxes", order = 1)]
public class BoxData : ScriptableObject {

	public BoxParams BoxInfo;

	//[Button]
	//public void OnEnable () {
	//	Init ();
	//}

	//private void Init () 
	//{
		
	//}

	[System.Serializable]
	public struct BoxParams {
		public BoxManager.BoxSize I_Size;
		public float F_Weight;
	}
}