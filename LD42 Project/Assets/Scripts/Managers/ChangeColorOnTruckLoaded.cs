﻿using UnityEngine;

[RequireComponent(typeof(Truck))]
public class ChangeColorOnTruckLoaded : MonoBehaviour
{
    [SerializeField] private Color loadedColor = Color.red;

    public Truck truck;

    private void Awake()
    {
        truck = GetComponent<Truck>();
    }

    private void OnEnable()
    {
        truck.TruckLoaded += Truck_TruckLoaded;
    }

    private void OnDisable()
    {
        truck.TruckLoaded -= Truck_TruckLoaded;
    }

    private void Truck_TruckLoaded()
    {
        GetComponent<Renderer>().material.SetColor("_BaseColor",loadedColor);
        GetComponent<Renderer>().material.color = loadedColor;
    }
}
