﻿using System;
using UnityEngine;

/// <summary>
/// Ross
/// Tracks current state of game.
/// Notifies when level is complete
/// Manages the trucks
/// </summary>
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public event Action<float> LevelComplete = delegate{};
    public event Action LevelFailed = delegate{};

    [SerializeField] private bool useLevelSettings;
    [SerializeField] private GameObject cratePrefab;
    [SerializeField] private GameObject truckParent;
    [SerializeField] private float truckLoadTimer = 20f;

    public LevelSettings levelSettings;
    public int trucksLoaded;
    public float totalCratesCollected;

    [HideInInspector] public float countdownTime;

    #region Powerups

    public bool PopeyeSpinach;
    public bool Shoes;
    public bool Trolley;

    #endregion

    #region Testing

    [Header("Testing")]

    [SerializeField] private KeyCode spawnCrateKey = KeyCode.S;
    [SerializeField] private Transform spawnPosition;

    #endregion

    public bool IsLevelComplete { get; private set; }

    private Truck[] trucks;
    private CrateSpawner[] crateSpawners;
    private bool levelComplete;

    #region UnityMethods

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);

        Initialise();
    }

    private void Initialise()
    {
        trucks = truckParent.GetComponentsInChildren<Truck>();
        crateSpawners = FindObjectsOfType<CrateSpawner>();

        if (useLevelSettings)
            ApplyLevelSettings();

        countdownTime = truckLoadTimer;
    }

    private void ApplyLevelSettings()
    {
        foreach (var truck in trucks)
        {
            truck.truckMaxWeight = levelSettings.TrucksMaxWeight;
        }
    }

    private void OnEnable()
    {
        foreach (Truck truck in trucks)
            truck.TruckLoaded += Truck_TruckLoaded;
    }

    private void OnDisable()
    {
        foreach (Truck truck in trucks)
            truck.TruckLoaded -= Truck_TruckLoaded;
    }

    private void Update()
    {
        if (levelComplete) return;

        countdownTime -= Time.deltaTime;

        if (countdownTime <= 0)
            FailLevel();
    }

    public void FailLevel()
    {
        levelComplete = true;
        LevelFailed();
    }

    public void CompletedLevel()
    {
        levelComplete = true;
        LevelComplete(0);
    }

    #endregion

    // public void SpawnCrates()
    // {
    //     foreach (var crateSpawner in crateSpawners)
    //         crateSpawner.SpawnCrate((CrateType)UnityEngine.Random.Range(0, 2));
    // }

    public void CollectedCrate()
    {
        totalCratesCollected++;
        countdownTime = truckLoadTimer;
    }

    private void Truck_TruckLoaded()
    {
        trucksLoaded++;

        Debug.Log(string.Format("{0} trucks loaded", trucksLoaded));

        if (trucksLoaded >= trucks.Length)
        {
            Debug.Log("Level Complete");
            LevelComplete(trucksLoaded);
            IsLevelComplete = true;
        }
    }

}
