﻿using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;

public class BoxManager : MonoBehaviour {
    public static BoxManager _BoxManager;

    public enum BoxSize { Small, Medium, Large };

 public bool B_Spawning = false;
 public float F_MaxDelay = 1f;
 public GameObject GO_BoxPrefabSmall = null;
 public GameObject GO_BoxPrefabMed = null;
 public GameObject GO_BoxPrefabLarge = null;
 public List<Transform> spawnLocations = new List<Transform> ();
 int spawnIndex = 0;

 [Range (0f, 1f)]
 public float F_SmallChance;
 [Range (0f, 1f)]
 public float F_MediumChance;
 [Range (0f, 1f)]
 public float F_LargeChance;

 public Vector2 WeightRange_S;
 public Vector2 WeightRange_M;
 public Vector2 WeightRange_L;

 private float F_CurrentDelay = 0f;
 public List<GameObject> ActiveBoxes = new List<GameObject> ();

 void Awake () {
 _BoxManager = this;
    }

    public void Start () {
        Invoke ("StartSpawns", 5);

        BoxHandler.PickedUp += RemoveBox;
        BoxHandler.Dropped += AddBox;
    }

    [Button]
    public void StartSpawns () {
        StartCoroutine (SpawnBoxes ());
        B_Spawning = true;
    }

    [Button]
    public void StopSpawns () {
        B_Spawning = false;
    }

    [Button]
    private void SpawnBox () {
        float F_Chance = Random.Range (0f, 1f);
        Debug.Log (F_Chance);
        if (spawnIndex + 1 < spawnLocations.Count) {
            spawnIndex++;
        } else {
            spawnIndex = 0;
        }

        BoxHandler BoxInfo = new BoxHandler ();

        if (F_Chance <= F_LargeChance) // Large Box
        {
            BoxInfo.I_Size = BoxSize.Large;
            BoxInfo.F_Weight = Random.Range (WeightRange_L.x, WeightRange_L.y);
        } else if (F_Chance <= F_MediumChance) // Medium Box
        {
            BoxInfo.I_Size = BoxSize.Medium;
            BoxInfo.F_Weight = Random.Range (WeightRange_M.x, WeightRange_M.y);
        } else // Small Box
        {
            BoxInfo.I_Size = BoxSize.Small;
            BoxInfo.F_Weight = Random.Range (WeightRange_S.x, WeightRange_S.y);
        }

        GameObject go_NewBox;
        if (BoxInfo.I_Size == BoxSize.Small) {
            go_NewBox = Instantiate (GO_BoxPrefabSmall, spawnLocations[spawnIndex].position, Quaternion.identity);
        } else if (BoxInfo.I_Size == BoxSize.Medium) {
            go_NewBox = Instantiate (GO_BoxPrefabMed, spawnLocations[spawnIndex].position, Quaternion.identity);
        } else {
            go_NewBox = Instantiate (GO_BoxPrefabLarge, spawnLocations[spawnIndex].position, Quaternion.identity);
        }

        BoxHandler newBoxInfo = go_NewBox.GetComponent<BoxHandler> ();
        newBoxInfo.I_Size = BoxInfo.I_Size;
        newBoxInfo.F_Weight = BoxInfo.F_Weight;
        go_NewBox.GetComponent<Rigidbody> ().mass = BoxInfo.F_Weight;

        ActiveBoxes.Add (go_NewBox);
    }

    private void AddBox (GameObject Box) {
        ActiveBoxes.Add (Box);
    }

    private void RemoveBox (GameObject Box) {
        ActiveBoxes.Remove (Box);
    }

    private IEnumerator SpawnBoxes () {
        while (true) {
            if (B_Spawning) {
                while (F_CurrentDelay <= F_MaxDelay) {
                    F_CurrentDelay += Time.deltaTime;
                    yield return null;
                }
                SpawnBox ();
                F_CurrentDelay = 0f;
            }
            yield return null;
        }
    }
}