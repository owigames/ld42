﻿using System;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    private void OnEnable()
    {
        GameManager.Instance.LevelFailed += Instance_LevelFailed;
    }

    private void OnDisable()
    {
        GameManager.Instance.LevelFailed -= Instance_LevelFailed;
    }

    private void Instance_LevelFailed()
    {
        CalculateScore();
        Debug.Log("YOU LOSE!");
    }

    private void CalculateScore()
    {
        var trucks = FindObjectsOfType<Truck>();

        float achievedWeight = 0;
        float desiredWeight = 0;

        foreach (var truck in trucks)
        {
            achievedWeight += truck.currentWeight;
            desiredWeight += truck.truckMaxWeight;
        }

        GetRanking(achievedWeight, desiredWeight);
    }

    private void GetRanking(float achievedWeight, float desiredWeight)
    {
        float pct = achievedWeight / desiredWeight;

        if (pct < 0.3f)
        {
            Debug.Log("Wow my gran is faster than that");
        }
        else if (pct < 0.6)
        {

        }
    }
}
