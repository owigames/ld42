﻿using UnityEngine;
using TMPro;

public class UI_DisplayWeight : MonoBehaviour
{
    private TextMeshPro text;
    private Truck truck;

    private void Awake()
    {
        text = GetComponent<TextMeshPro>();
        truck = GetComponentInParent<Truck>();
    }

    void Update()
    {
        text.text = string.Format("{0} / {1}", truck.currentWeight, truck.truckMaxWeight);
    }
}
