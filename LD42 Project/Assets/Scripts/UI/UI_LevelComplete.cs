﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_LevelComplete : MonoBehaviour
{
    private void OnEnable()
    {
        GameManager.Instance.LevelComplete += Instance_LevelComplete;
    }

    private void OnDisable()
    {
        GameManager.Instance.LevelComplete -= Instance_LevelComplete;
    }

    private void Instance_LevelComplete(float trucksLoaded)
    {
        ShowLevelCompleteUI(trucksLoaded);
    }

    private void ShowLevelCompleteUI(float trucksLoaded)
    {
        GetComponent<TextMeshProUGUI>().text = "complete!";
    }
}
