﻿using UnityEngine;
using TMPro;

public class UI_CratesCollected : MonoBehaviour
{
    private TextMeshProUGUI text;

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        text.text = string.Format("{0} Crates", GameManager.Instance.totalCratesCollected);
    }
}
