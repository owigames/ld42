﻿using UnityEngine;
using TMPro;

public class UI_LevelTimer : MonoBehaviour
{
    private TextMeshProUGUI text;

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        text.text = string.Format("{0} seconds remaining", Mathf.RoundToInt(GameManager.Instance.countdownTime));
    }
}
