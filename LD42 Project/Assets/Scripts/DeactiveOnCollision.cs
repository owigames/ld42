﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactiveOnCollision : MonoBehaviour {

    [SerializeField] private string TagToCheck;
    private void OnCollisionEnter (Collision collision) {
        if (collision.gameObject.tag == TagToCheck) {
            collision.gameObject.tag = "Untagged";
            if (collision.gameObject.GetComponent<Rigidbody> () != null) {
                collision.gameObject.GetComponent<BoxHandler> ().Drop(true);
                Destroy (collision.gameObject.GetComponent<Rigidbody> ());
            }
        }
    }
}