﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public enum PlayerSelection {
	Player1,
	Player2,
	Player3,
	Player4
};

public class PlayerMovement : MonoBehaviour {

	//---PickUp & Drop---
	public bool didPickUp;
	public Transform pickUpLocation;
	private Collider _collider;
	private Collider intersectCollider;
	public GameObject pickUpTarget;
	public LayerMask canPickUp;
	public Collider[] hitobj;

	[SerializeField] string PicUpObjTag;

	public PlayerSelection _PlayerSelection;
	public int playerID;

    Animator _animator;
	Player _Input;

    public INavigation NavigationCommand;

	void Awake () {
	    playerID = (_PlayerSelection.GetHashCode ());
	    _Input = ReInput.players.GetPlayer (playerID);
	    _animator = GetComponent<Animator> ();
        NavigationCommand = new BasicMovement(_animator, transform);
    }

    void Start () {
		didPickUp = false;
		_collider = pickUpLocation.GetComponent<Collider> ();
        //---set pickupables to array of pickupables from gamemanager...
    }

	void Update () {

		//Attack
		InteractCtrl ("Interact");

		//Run
		RunCtrl ("Run");

		//Move
		MovementCtrl (_Input.GetAxis ("Horizontal"), _Input.GetAxis ("Vertical"));

	}

	void InteractCtrl (string button) {
		if (_Input.GetButtonDown (button)) {
			_animator.SetBool ("Attacking", true);

			//---YOU NEED TO DROP---
			if (didPickUp) {
				pickUpTarget.GetComponent<Rigidbody> ().isKinematic = false;
				pickUpTarget.GetComponent<Collider> ().enabled = true;
				pickUpLocation.transform.DetachChildren ();
				didPickUp = false;

				Vector3 dir = transform.forward;
				pickUpTarget.GetComponent<Rigidbody> ().AddForce (dir * 10.2f);
				pickUpTarget.GetComponent<BoxHandler> ().Drop ();
			}

			//---YOU NEED TO PICKUP---
			else if (!didPickUp) {
				hitobj = Physics.OverlapSphere (pickUpLocation.position, 2, canPickUp);

				if (hitobj.Length > 0) {
					pickUpTarget = hitobj[0].gameObject;
					//---compare tag...
					if (pickUpTarget.tag == PicUpObjTag) {

						pickUpTarget.transform.position = pickUpLocation.position;
						pickUpTarget.GetComponent<Collider> ().enabled = false;
						pickUpTarget.GetComponent<Rigidbody> ().isKinematic = true;
						pickUpTarget.transform.SetParent (pickUpLocation.transform);
						pickUpTarget.GetComponent<BoxHandler> ().PickUp ();

						didPickUp = true;
					}
				}

			}

		} else if (_Input.GetButtonUp (button)) {
			_animator.SetBool ("Attacking", false);
		}
	}

	void RunCtrl (string button) {
		if (_Input.GetButtonDown (button)) {
			_animator.SetBool ("Running", true);
		} else if (_animator.GetBool ("Running") && _Input.GetButtonUp (button)) {
			_animator.SetBool ("Running", false);
		}
	}

	void MovementCtrl (float H, float V) {
        NavigationCommand.Nav(H, V);
    }
}
public interface INavigation
{
    void Nav(float Horizontal, float Vertical);
}
public class BasicMovement : INavigation
{
    Animator _animator;
    Transform _transform;
    void INavigation.Nav(float Horizontal, float Vertical)
    {
        if (Horizontal != 0 || Vertical != 0)
        {

            //Move
            _animator.SetBool("Moving", true);

            //Face Analog Direction
            float myAngle = Mathf.Atan2(Horizontal, Vertical) * Mathf.Rad2Deg;
            _transform.eulerAngles = new Vector3(0, myAngle, 0);

        }
        else
        {
            _animator.SetBool("Moving", false);
        }
    }
    public BasicMovement(Animator animator,Transform transform)
    {
        _animator = animator;
        _transform = transform;
    }
}
public class SharedMovement : INavigation
{
    Animator _animator;
    Transform _transform;
    Player _otherPlayer;

    public SharedMovement(Animator animator, Transform transform, Player otherPlayer)
    {
        _animator = animator;
        _transform = transform;
        _otherPlayer = otherPlayer;
    }

    void INavigation.Nav(float Horizontal, float Vertical)
    {
        if (_otherPlayer == null)
        {
            if (Horizontal != 0 || Vertical != 0)
            {

                //Move
                _animator.SetBool("Moving", true);
                _animator.speed = 0.5f;

                //Face Analog Direction
                float myAngle = Mathf.Atan2(Horizontal, Vertical) * Mathf.Rad2Deg;
                _transform.eulerAngles = new Vector3(0, myAngle, 0);

            }
            else
                _animator.SetBool("Moving", false);
        }
        else
        {
            float newHorizontal = (Horizontal + _otherPlayer.GetAxis("Horizontal")) / 2;
            float newVertical = (Vertical + _otherPlayer.GetAxis("Vertical")) / 2;

            if (newHorizontal != 0 || newVertical != 0)
            {
                //Face Analog Direction
                float myAngle = Mathf.Atan2(Horizontal, Vertical) * Mathf.Rad2Deg;
                _transform.eulerAngles = new Vector3(0, myAngle, 0);
            }
            else
                _animator.SetBool("Moving", false);
        }
    }
}
