﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLookAt : MonoBehaviour {
	
	public Transform target;

	// Use this for initialization
	void Start () {
		target = Camera.main.transform;
	}
	
    
    void Update ()
    {
        transform.LookAt(target);
    }

}
