﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGravity : MonoBehaviour {

	Rigidbody _rigidBody;
	public float forceAmount = 50;

	void Start () {
		_rigidBody = GetComponent<Rigidbody> ();
	}

	void LateUpdate () {
		_rigidBody.AddForce (Vector3.down * Time.deltaTime * forceAmount, ForceMode.VelocityChange);
	}
}