﻿using System;
using UnityEngine;

public class Truck : MonoBehaviour {
    public event Action TruckLoaded = delegate { };

    public float truckMaxWeight = 50f;
    public float currentWeight;
    private bool truckLoaded;

    private void OnTriggerEnter (Collider other) {
        var crate = other.GetComponent<BoxHandler> ();

        if (crate != null) {
            GameManager.Instance.CollectedCrate ();
            currentWeight += crate.F_Weight;
            CheckWeight ();
            Destroy(other.gameObject);
        }
    }

    private void CheckWeight () {
        if (currentWeight >= truckMaxWeight && !truckLoaded) {
            GetComponent<Collider> ().enabled = false;
            truckLoaded = true;
            TruckLoaded ();
        }
    }
}