﻿using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;

public class BoxHandler : MonoBehaviour {
    public BoxManager.BoxSize I_Size;
    public float F_Weight;

    public delegate void BoxAction (GameObject Box);
    public static event BoxAction PickedUp;
    public static event BoxAction Dropped;

    ConveyorBelt _conveyorBelt;

    public bool onConveyor = false;
    public bool pickedUp = false;

    private void Update () {
        if (onConveyor && !pickedUp) {
            transform.Translate (_conveyorBelt.transform.forward * _conveyorBelt.boxSpeed * Time.deltaTime, Space.World);
        }
    }

    [Button]
    public void PickUp () {
        if (PickedUp != null) PickedUp (gameObject);
        pickedUp = true;
        onConveyor = false;
    }

    [Button]
    public void Drop (bool floor = false) {
        if (Dropped != null) Dropped (gameObject);
        if (floor) onConveyor = false;
        pickedUp = false;
    }

    void OnCollisionStay (Collision other) {
        if (other.gameObject.layer == 10 && pickedUp == false && onConveyor == false) {
            onConveyor = true;
            _conveyorBelt = other.transform.GetComponent<ConveyorBelt> ();
        }
    }

    void OnCollisionExit (Collision other) {
        if (other.gameObject.layer == 10) {
            onConveyor = false;
        }
    }
}