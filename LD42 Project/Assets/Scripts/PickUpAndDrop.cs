﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpAndDrop : MonoBehaviour
{
    [Header("Pick Up & Drop")]
    public bool didPickUp;
    private GameObject pickUpLocation;
    private Collider _collider;
    private Collider intersectCollider;
    public GameObject pickUpTarget;
    public GameObject[] pickupables;
    

   // public List<> canPickUp = new li;

    void Start ()
    {
        pickUpLocation = this.gameObject;
        didPickUp = false;
        _collider = this.GetComponent<Collider>();
        //---set pickupables to array of pickupables from gamemanager...
	}
	
	void Update ()
    {
        //---CHECK INPUT---
        if (Input.GetKeyDown(KeyCode.Mouse0))
        //if (Input.GetButtonDown("Pick Up"))
        {
            //---YOU NEED TO DROP---
            if (didPickUp)
            {
                pickUpTarget.GetComponent<Rigidbody>().isKinematic = false;
                this.transform.DetachChildren();
                didPickUp = false;
            }


            //---YOU NEED TO PICKUP---
            else if (!didPickUp)
            {
                //---CHECK EACH PICKUPABLE OBJECT---
                for (int i = 0; i < pickupables.Length; i++)
                {
                    intersectCollider = pickupables[i].GetComponent<Collider>();
                    //---IN AREA?---
                    if (_collider.bounds.Intersects(intersectCollider.bounds))
                    {

                      //  canPickUp[i].ad
                        pickUpTarget = pickupables[i];
                        pickUpTarget.transform.position = pickUpLocation.transform.position;
                        pickUpTarget.GetComponent<Rigidbody>().isKinematic = true;
                        pickUpTarget.transform.SetParent(pickUpLocation.transform);

                        didPickUp = true;

                        break;
                    }
                }
            }

        }
    }
}
