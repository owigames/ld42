﻿using UnityEngine;

public class Crate : MonoBehaviour, ICrate
{
    [SerializeField] private float weight = 5;

    public bool IsHeavy;

    float ICrate.GetWeight()
    {
        return weight;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Truck>() != null)
        {
            Destroy(gameObject);
        }
    }
}
