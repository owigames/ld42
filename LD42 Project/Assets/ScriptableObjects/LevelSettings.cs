﻿using UnityEngine;

[CreateAssetMenu(menuName = "LevelSettings")]
public class LevelSettings : ScriptableObject
{
    public float TrucksMaxWeight;
    public float LevelTime;
}
